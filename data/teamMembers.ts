export const teamMembers = [
  {
    name: "Gombi",
    title: "Tech lead",
    weakness: "Alcohol",
  },
  {
    name: "Akos",
    title: "Web developer",
    weakness: "Too much working out",
  },
  {
    name: "Brezo",
    title: "Tech lead",
    weakness: "Cats",
  },
  {
    name: "Marci",
    title: "Cool web developer",
    weakness: "Dogs",
  },
  {
    name: "Reni",
    title: "Web developer",
    weakness: "Manga",
  },
];
