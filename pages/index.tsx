import Head from 'next/head';
import { gql, useQuery } from '@apollo/client';
import Link from 'next/link';
import  {TeamMembers}  from '../components/TeamMembers';

const Team = gql`
  query {
    teamMembers {
      name
      weakness
      title
    }
  }
`;

function Home() {
  const { data, loading, error } = useQuery(Team);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Oh no... {error.message}</p>;
  console.log(data);

  return (
    <div>
      <div>das</div> 
      <Head>
        <title>TeamMembers</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container mx-auto max-w-5xl my-20 px-5">
        <div>
         {TeamMembers(data)}
        </div>
      </div>
    </div>
  );
}

export default Home;
