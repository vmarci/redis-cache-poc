import { ApolloServer } from 'apollo-server-micro';
import { typeDefs } from '../../graphql/schema';
import Cors from 'micro-cors';
import { resolvers } from '../../graphql/resolvers'
import { BaseRedisCache } from 'apollo-server-cache-redis'
import Redis from 'ioredis'
import { MicroRequest } from 'apollo-server-micro/dist/types';
import { ServerResponse } from 'http';
import Keyv from "keyv";
import { KeyvAdapter } from "@apollo/utils.keyvadapter";
 

const cors = Cors();

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  csrfPrevention: true,
  cache: new KeyvAdapter(new Keyv('redis://default:asd@localhost:6379')), 
});



const startServer = apolloServer.start();

export default cors(async function handler(req: MicroRequest, res: ServerResponse) {
  if (req.method === 'OPTIONS') {
    res.end();
    return false;
  }
  await startServer;

  await apolloServer.createHandler({
    path: '/api/graphql',
  })(req, res);
});

export const config = {
  api: {
    bodyParser: false,
  },
};
