

import {gql} from 'apollo-server-micro';




export const typeDefs = gql `
 type TeamMember {
  id: String
  title: String
  name: String
  weakness: String
 }

type Query {
  teamMembers: [TeamMember]!
}

type Mutation {
  createTeamMember(options: CreateTeamMemberInput): TeamMember
}

input CreateTeamMemberInput {
  name: String!
  title: String!
  weakness: String!
}

`;

