import { PrismaClient } from '@prisma/client'
import { PrismaClientOptions } from '@prisma/client/runtime'
import prisma from '../lib/prisma'

export const resolvers = {
    Query: {
        teamMembers: async () => await prisma.teamMember.findMany()
    },
    Mutation: {
        createTeamMember: async () => await prisma.teamMember.create({
            data: {
                name: 'exampleName',
                title: 'exampleTitle',
                weakness: 'exampleweakness'
            }
        })
    }
}